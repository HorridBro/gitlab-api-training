#!/usr/bin/python3

import argparse
import requests
import json

BASE_URL = "https://gitlab.com/api/v4/projects/{0}"


def get_all_commits_nr(project_id):
    url = BASE_URL.format("{0}/repository/commits".format(project_id))
    commits = []
    page_nr = 1
    while True:
        params = {
            "per_page": 100,
            "page": page_nr,
            "all": True
        }
        r = requests.get(url, params=params)
        data = json.loads(r.text)
        commits.extend(data)
        page_nr = r.headers["X-Next-Page"]
        if page_nr == "":
            break
    return len(data)


def get_merge_requests(project_id):
    url = BASE_URL.format("{0}/merge_requests".format(project_id))
    merge_requests = []
    page_nr = 1
    while True:
        params = {
            "scope": "all",
            "per_page": 100,
            "page": page_nr
        }
        r = requests.get(url, params=params)
        data = json.loads(r.text)
        merge_requests.extend(data)
        page_nr = r.headers["X-Next-Page"]
        if page_nr == "":
            break
    return data


def get_project_id(project_url):
    # encode '/' with '%2F'
    escaped_project_url = requests.utils.quote(project_url, safe="")
    full_url = BASE_URL.format(escaped_project_url)
    r = requests.get(full_url)
    data = json.loads(r.text)
    project_id = data["id"]
    return project_id


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--project_path",
                        help="Path to the project e.g. for the link: https://gitlab.com/gitlab-org/gitlab "
                             "the value here should be 'gitlab-org/gitlab'",
                        default="gitlab-org/gitlab")
    parser.add_argument("-c", "--commits_nr",
                        help="Get all number of commits made by each user for each branch available",
                        action="store_true")
    parser.add_argument("-m", "--merge_requests",
                        help="List all merge requests",
                        action="store_true")
    args = parser.parse_args()
    project_id = get_project_id(args.project_path)
    if args.merge_requests:
        print("List all merge requests")
        merge_requests = get_merge_requests(project_id)
        print(json.dumps(merge_requests, sort_keys=True, indent=4))
        print("")
    if args.commits_nr:
        print("Number of all commits")
        nr_commits = get_all_commits_nr(project_id)
        print(nr_commits)
        print("")
