# gitlab-api-training

## How to run

### On this repo

##### Get number of all commits
``
python3 main.py -p HorridBro/gitlab-api-training -c
``

##### List all merge requests 

``
python3 main.py -p HorridBro/gitlab-api-training -m
``


### Another repo `ercom/livebootp`

``
python3 main.py -p ercom/livebootp -c
``


``
python3 main.py -p ercom/livebootp -m
``